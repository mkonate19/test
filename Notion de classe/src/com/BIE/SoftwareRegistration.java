package com.BIE;

public class SoftwareRegistration
{
    /* Constructeur  */
    public SoftwareRegistration(int expiration)
    {
        this.mExpirationYear = expiration;
        System.out.println("Enregistrement du produit, valide jusqu'en " + this.mExpirationYear);
    }

    private int mExpirationYear;

    /* GETTERS  */
    public int getmExpirationYear() {
        return this.mExpirationYear;
    }

    /* SETTERS  */
    public void setmExpirationYear(int expiration) {
        this.mExpirationYear = expiration;
    }
}
