package com.BIE.Quizz;

import java.util.*;
public class Reponse
{
    public String reponse;
    public Boolean isGood;

    Reponse(String reponse, boolean isGood)
    {
        this.reponse = reponse;
        this.isGood = isGood;
    }
    public void showReponse()
    {
        System.out.println("La bonne réponse est : " + reponse);
    }


}
