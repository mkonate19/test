package com.BIE.Quizz;

import java.util.*;


public class Quizz
{
    public String name;
    public ArrayList<Question>questions = new ArrayList<Question>();
    public Iterator<Question>iterateur;
    Score sc = new Score(0, 5);


    Quizz(String nom)
    {
        this.name = nom;
        initQuestionrep();
    }
    public void StartQuizz()
    {
        System.out.println("----------------------------------------------------------");
        System.out.println("Bonjour "+ this.name +" le quizz commence pour vous !!!");
        System.out.println("----------------------------------------------------------\n");

        for(int number_questions = 0; iterateur.hasNext(); number_questions++)
        {
            System.out.println("------------------------ Question "+ number_questions +" ------------------------");
            iterateur.next().setReponse(this.sc);
        }
        sc.showScore();
    }

    public void initQuestionrep()
    {

        questions.add(new Question("Est-ce que Nitharshan est nul en Réseau ?\na) Oui\nb) Non", new Reponse[]{new Reponse("a",false),new Reponse("b",true)}));
        questions.add(new Question("Morike est dans le groupe ?\na) Oui\nb) Non\n", new Reponse[]{new Reponse("a",false),new Reponse("b",true)}));
        questions.add(new Question("Le java est nul ?\na) Oui\nb) Non\n", new Reponse[]{new Reponse("a", true), new Reponse("b", false)}));
        questions.add(new Question("2 + 2 ?\na) 22\nb) 4\nc) 10", new Reponse[]{new Reponse("a",false),new Reponse("b",true),new Reponse("c",false)}));
        questions.add(new Question("1 + 0 ?\na) 1\nb) 0\nc) 3\nd) 8", new Reponse[]{new Reponse("a",true),new Reponse("b",false),new Reponse("c",false),new Reponse("d",false)}));
        iterateur = questions.iterator();
    }

}
