package com.BIE.Quizz.Prof.tp1;

import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello World !");

        ArrayList<Reponse> question1reponses = new ArrayList<Reponse>();
        Reponse question1Reponse1 = new Reponse("1. Staline",true);
        Reponse question1Reponse2 = new Reponse("2. Trotski",false);
        Reponse question1Reponse3 = new Reponse("3. Lénine",false);
        Reponse question1Reponse4 = new Reponse("4. Molotov",false);
        question1reponses.add(question1Reponse1);
        question1reponses.add(question1Reponse2);
        question1reponses.add(question1Reponse3);
        question1reponses.add(question1Reponse4);

        Question question1 = new Question("A. Quel célèbre dictateur dirigea l’URSS du milieu des années 1920 à 1953 ?",question1reponses);

        ArrayList<Reponse> question2reponses = new ArrayList<Reponse>();
        Reponse question2Reponse1 = new Reponse("1. La France",false);
        Reponse question2Reponse2 = new Reponse("2. L'Italie",false);
        Reponse question2Reponse3 = new Reponse("3. L'Espagne",true);
        Reponse question2Reponse4 = new Reponse("4. Le Portugal",false);
        question2reponses.add(question2Reponse1);
        question2reponses.add(question2Reponse2);
        question2reponses.add(question2Reponse3);
        question2reponses.add(question2Reponse4);

        Question question2 = new Question("B. Dans quel pays peut-on trouver la Catalogne, l’Andalousie et la Castille ?",question2reponses);

        ArrayList<Reponse> question3reponses = new ArrayList<Reponse>();
        Reponse question3Reponse1 = new Reponse("1. L'Argentine",false);
        Reponse question3Reponse2 = new Reponse("2. L'Italie",false);
        Reponse question3Reponse3 = new Reponse("3. L'Espagne",false);
        Reponse question3Reponse4 = new Reponse("4. L'Allemagne",true);
        question3reponses.add(question3Reponse1);
        question3reponses.add(question3Reponse2);
        question3reponses.add(question3Reponse3);
        question3reponses.add(question3Reponse4);

        Question question3 = new Question("C. Quel pays a remporté la coupe du monde de football en 2014 ?",question3reponses);

        ArrayList<Reponse> question4reponses = new ArrayList<Reponse>();
        Reponse question4Reponse1 = new Reponse("1. Albert Einstein",false);
        Reponse question4Reponse2 = new Reponse("2. Alan Turing",true);
        Reponse question4Reponse3 = new Reponse("3. Marc Andreessen",false);
        Reponse question4Reponse4 = new Reponse("4. Ibrahim Gharbi",false);
        question4reponses.add(question4Reponse1);
        question4reponses.add(question4Reponse2);
        question4reponses.add(question4Reponse3);
        question4reponses.add(question4Reponse4);

        Question question4 = new Question("D. Qui a créé Enigma ?",question4reponses);

        ArrayList<Reponse> question5reponses = new ArrayList<Reponse>();
        Reponse question5Reponse1 = new Reponse("1. File Transmission Protocol",false);
        Reponse question5Reponse2 = new Reponse("2. File Transfer Protocol",true);
        Reponse question5Reponse3 = new Reponse("3. Fiber Twisted Pairs",false);
        Reponse question5Reponse4 = new Reponse("4. Aucune des réponses ci-dessus",false);
        question5reponses.add(question5Reponse1);
        question5reponses.add(question5Reponse2);
        question5reponses.add(question5Reponse3);
        question5reponses.add(question5Reponse4);

        Question question5 = new Question("E. Que veut dire FTP ?",question5reponses);

        ArrayList<Question> listQuestionQuizz1 = new ArrayList<Question>();
        listQuestionQuizz1.add(question1);
        listQuestionQuizz1.add(question2);
        listQuestionQuizz1.add(question3);
        listQuestionQuizz1.add(question4);
        listQuestionQuizz1.add(question5);

        Quizz quizz1 = new Quizz("quizz 1",listQuestionQuizz1);
        quizz1.startQuizz();

    }


}