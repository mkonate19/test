package com.BIE.Quizz;

import java.util.*;

public class Question
{
    public String mesQuestion;
    public Reponse[] mesReponse;
    public Scanner rep = new Scanner(System.in);


    Question(String mesQuestion, Reponse[]mesReponse)
    {
        this.mesQuestion = mesQuestion;
        this.mesReponse = mesReponse;
    }


    public void setReponse(Score sc)
    {
        System.out.println(mesQuestion);
        System.out.println("entrez votre reponse : ");
        String reponse_donne = rep.nextLine();

        for(Reponse key : mesReponse)
        {
            if(reponse_donne.equals(key.reponse))
            {
                if(key.isGood)
                {
                    System.out.println("VRAI !");
                    sc.addScore();
                    break;
                }
            }
            else
            {
                if(key.isGood)
                {
                    System.out.println("FAUX !");
                    key.showReponse();
                    break;
                }
            }
        }
    }
}
