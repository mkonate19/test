package com.BIE;

public class Zoo1 {

    String nom;
    Double taille;
    int age;
    Boolean vie;
    String cri;
    int poids;
    Boolean etat_espece;

    public static void main(String[] args) {

        Zoo1 lion = new Zoo1("Lion", 2.3 ,34, true, "Graaaaaaaaa", 100,  true);
        Zoo2 lion2 = new Zoo2("Lion2", 2.3 ,34, true, "Graaaaaaaaa", 100,  true);
        lion.crier();
        lion.vieillir();
        lion2.crier();
        lion2.vieillir();

    }

    public Zoo1(String nom, Double taille, int age, Boolean vie, String cri, int poids, Boolean etat_espece) {
        this.nom = nom;
        this.taille = taille;
        this.age = age;
        this.vie = vie;
        this.cri = cri;
        this.poids = poids;
        this.etat_espece = etat_espece;
    }
    void crier() {
        System.out.println("Le cri de l'animal est: "+ cri);
    }

    void vieillir() {
        if(vie!=true) {
            System.out.println("L'animal n'est pas en vie");
        } else {
            System.out.println("L'animal vie et viellie");
        }
    }
}
