package com.BIE;


public class Zoo {
    public String nom;
    public String taille;
    public int age;
    public int dureedevie;
    public String cri;
    public Double poids;
    public boolean etat;
    

    Zoo(String lecri, int lage, int ladureedevie) {
        cri = lecri;
        age = lage;
        dureedevie = ladureedevie;
    }
    public static void main(String[] args){
        Zoo tigre = new Zoo("graaaaaaaaaaaaa", 31, 30);
        
        tigre.crier();
        tigre.vieillir();
        tigre.dureedevie();
        
    }

    public void crier() {
        System.out.println(cri);
    }

    public void vieillir() {
        if(age>dureedevie) {
            System.out.println("L'animal vieilli");
        }
    }

    public void dureedevie() {
        System.out.println(dureedevie);
    }
}

// Zoo elephant = new Zoo();
// Zoo zebre = new Zoo();
