package com.BIE.Course;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Simulation
{
	public static void main(String[] args)
	{
		// Les voitures pour la course
		List<Vehicule> vehicules = new ArrayList<Vehicule>();
		vehicules.add(new Vehicule("Mercedes"));
		vehicules.add(new Vehicule("BMW"));
		vehicules.add(new Vehicule("Audi"));
		vehicules.add(new Vehicule("Porsche"));



		//Paramètres de la course <
		double distance = 40.0 ;
		int nbtours = 3 ;
		int time = 60;
		Course c = new Course (distance, time, nbtours, vehicules);

		int nb = 1 ; // Pour le temps

		while(!c.etat_course()){ //tant que la course n'est pas finie
			clearConsole();
			System.out.println("Après "+ nb*(time/60) +" minutes : \n");
			c.course();
			c.changer_vitesse();
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			nb++;
		}

		System.out.println("\nLa course est terminée !!!\n\nLa "+c.gagnant().nom +" a remporté cette course !");
	}
	public static void clearConsole() {//méthode qui permet de clear l'écran entre chaque affichage.
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}