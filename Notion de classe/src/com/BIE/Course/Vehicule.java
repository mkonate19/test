package com.BIE.Course;



public class Vehicule{
    String nom;
    static final Double vitesse_max = 230.0;
    static final Double vitesse_min = 50.0;
    Double vitesse_actu;
    Double distance_parcourue = 0.0 ;

    public Vehicule(String nom) {
        this.nom = nom;
        this.calcul_vitesse(); //la vitesse de départ est aléatoirement choisie entre min et max
    }

    /*
     Calcul_vitesse  : change la vitesse de la voiture en prenant une vitesse aléatoire
      entre max et min
    */
    public void calcul_vitesse()
    {
        this.vitesse_actu = (double) (Math.random()*(vitesse_max-vitesse_min+1)+vitesse_min) ;
    }

    /*
		setDistance_parcourue : remet à jour la distance parcourue par la voiture
    	en tenant compte de sa vitesse et du temps pendant lequel elle a circulé à cette vitesse

    	v = delta l / delta t => l = v * delta t

    	- la vitesse est y en km/h il faut le convertir en km/s
    */

    public void setDistance_parcourue(int time)
    {
        this.distance_parcourue += (time*this.vitesse_actu/3600);
    }

    /*
     toString pour afficher les informations d'une voiture
    */
    public String toString(){
        return "La "+this.nom+" : distance parcourue : "+ this.distance_parcourue + " Vitesse : "+this.vitesse_actu ;
    }
}