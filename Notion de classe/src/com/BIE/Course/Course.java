package com.BIE.Course;


import java.util.*;

public class Course
{
    // distance du circuit
    public double distance_circuit;
    //nombre de tours de la course 
    public int nbtours;
    // Temps pour le changement de la vitesse des voitures
    public int time;
    //Liste des voitures participants
    public List<Vehicule> vehicules = new ArrayList<Vehicule>();

    public Course(double distance_circuit, int time,  int nbtours, List<Vehicule> vehicules)
    {
        this.distance_circuit = distance_circuit;
        this.nbtours = nbtours;
        this.time = time ;
        this.vehicules = vehicules;
    }

    /*Cette fonction permet de changer la vitesse de toutes voitures de la course 
        et de remettre à jour la distance parcourue par les voitures
    */
    public void changer_vitesse()
    {
        for(Vehicule v : vehicules) {
            v.setDistance_parcourue(this.time);
            v.calcul_vitesse();
        }
    }


    /*
    etat_course : définit si la course est finie ou pas
    Dès que l'une des voitures parcourt la distance nécessaire, la course est finie. 
    Elle retourne Vrai si la course est finie et Faux sinon
    */
    public boolean etat_course()
    {
        for (Vehicule v : vehicules){
            if(v.distance_parcourue >= this.distance_circuit * nbtours)
                return true;
        }
        return false;
    }

    /*
    Détermine le gagnant de la course. La voiture qui parcourera la distance nécessaire en premier. 
        120 km avec les paramètres de base car 40 km * 3 tours
        */
    public Vehicule gagnant()
    {
        Vehicule gagnant  = this.vehicules.get(0) ;
        for(Vehicule v : this.vehicules) {
            if(gagnant.distance_parcourue < v.distance_parcourue)
                gagnant = v ;
        }

        return gagnant ;
    }


    /*
    course affiche l'état de la course : les véhicules avec leurs caractéristiques. 
    */
    public void course (){

        for(Vehicule v : this.vehicules) {
            System.out.println(v);
        }
    }



}