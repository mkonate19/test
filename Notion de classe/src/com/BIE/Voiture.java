package com.BIE;

public class Voiture
{
	int vitesse;
	String nom;

	public static void main(String[] args) {
        
        Voiture n = new Voiture(2, "Yaris");
		n.describe();
	}
	public Voiture(int vitesse, String nom) {
		this.vitesse = vitesse;
		this.nom = nom;
	}

	void describe() {
		System.out.println("Je m'appelle "+ nom);
	}
}
