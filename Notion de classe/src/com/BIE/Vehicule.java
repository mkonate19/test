package com.BIE;

public class Vehicule
{
     String m_Nom;
     int m_Age;

    public static void main(String[] args)
    {
        Vehicule Toyota = new Vehicule("Yaris", 6);
        Vehicule Mercedes = new Vehicule("G20", 34);
        Toyota.describe();
        Mercedes.describe();
    }

    public Vehicule(String nom, int age)
    {
        this.m_Nom = nom;
        this.m_Age = age;
    }

    public void describe()
    {
        System.out.println(this.m_Nom + " - " + this.m_Age);
    }
}
