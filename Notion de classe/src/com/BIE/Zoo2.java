package com.BIE;

public class Zoo2 extends Zoo1{


    public Zoo2(String nom, Double taille, int age, Boolean vie, String cri, int poids, Boolean etat_espece) {
        super(nom, taille, age, vie, cri, poids, etat_espece);
    }


    @Override
    void crier() {
        super.crier();
        System.out.println("L'animal est dans le Zoo2 aussi" + nom + cri);
    }
}
